import Vue from 'vue'
import InfiniteLoading from 'vue-infinite-loading'

export default ({ app }) => {
    Vue.use('infinite-loading', InfiniteLoading)
}